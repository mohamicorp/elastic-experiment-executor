import json
import os
import string
import uuid
from random import randint
from collections import defaultdict

config_file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'confluence-macro.json'))
with open(config_file_path, "r") as config_file:
    support_macro_list = json.loads(config_file.read())


def macro_to_rests_mapping(test_script):
    """
    This method contains a map which map between macro and rest requests need to be call in additional of normal page
    view rest call
    :param test_script:
    :return: a map between macro (name) and url for rest call
    """
    macro_to_rests_map = dict()
    macro_to_rests_map["contributors"] = lambda space_key, page_id: \
        "/rest/com.atlassian.confluence.contributors/1.0/contributors?title=test&contextEntityId=" + page_id
    macro_to_rests_map["detailssummary"] = lambda space_key, page_id: \
        "/rest/masterdetail/1.0/detailssummary/lines?" \
        "pageSize=30&pageIndex=0&cql=+label+%3D\"resource_page\"&spaceKey=" + space_key + \
        "&contentId=" + page_id + "&countComments=false&countLikes=false&sortBy=&reverseSort="

    return macro_to_rests_map


def macro_to_editor_mapping(test_script):
    """
    This method contains a map which map between macro and macro editor format
    :param test_script:
    :return:  map which map between macro and macro editor format
    """
    macro_to_editor_map = dict()
    macro_to_editor_map['details'] = '<table class=\"wysiwyg-macro\"' \
                                     'style=\"background-image: url(\'{base_url}/plugins/servlet/confluence/placeholder/macro-heading?definition=e2RldGFpbHN9&amp;locale=en_GB&amp;version=2\'); background-repeat: no-repeat;\"' \
                                     'data-macro-name=\"{macro_name}\"' \
                                     'data-macro-id=\"' + str(uuid.uuid1()) +'\" ' \
                                     'data-macro-schema-version=\"1\"' \
                                     'data-macro-body-type=\"RICH_TEXT\">' \
                                     '<tbody><tr><td class=\"wysiwyg-macro-body\">' \
                                     '<table class=\"wrapped confluenceTable\">' \
                                     '<colgroup><col /><col /></colgroup> <tbody>' \
                                     '<tr><td class=\"confluenceTd\">Deadline</td><td class=\"confluenceTd\">1 June</td></tr>' \
                                     '<tr><td class=\"confluenceTd\">Current Status</td><td class=\"confluenceTd\">Not Start</td></tr>' \
                                     '</tbody></table></td></tr></tbody></table>'
    macro_to_editor_map['excerpt'] = '<table class=\"wysiwyg-macro\"' \
                                     'style=\"background-image: url(\'{base_url}/plugins/servlet/confluence/placeholder/macro-heading?definition=e2V4Y2VycHQ6YXRsYXNzaWFuLW1hY3JvLW91dHB1dC10eXBlPUlOTElORX0&locale=en_GB&version=2&amp;locale=en_GB&amp;version=2\'); background-repeat: no-repeat;\"' \
                                     'data-macro-name=\"{macro_name}\"' \
                                     'data-macro-id=\"' + str(uuid.uuid1()) +'\" ' \
                                     'data-macro-schema-version=\"1\"' \
                                     'data-macro-body-type=\"RICH_TEXT\">' \
                                     '<tbody><tr><td class=\"wysiwyg-macro-body\">' \
                                     '<p>reuse able text ...</p></td></tr></tbody></table>'
    macro_list = ['toc', 'index', 'status', 'contributors', 'livesearch']
    for macro in macro_list:
        # base_url, macro_name, parameters, default_parameters
        macro_to_editor_map[macro] = '<p>' \
                                     '<img class=\"editor-inline-macro\" ' \
                                     'height=\"18\" width=\"88\" ' \
                                     'src=\"{base_url}/plugins/servlet/status-macro/placeholder?title=test\" ' \
                                     'data-macro-name=\"{macro_name}\" ' \
                                     'data-macro-parameters=\"title=test\" ' \
                                     'data-macro-schema-version=\"1\" /></p>'
    report_macro_list = ['detailssummary', 'excerpt-include']
    for macro in report_macro_list:
        macro_to_editor_map[macro] = '<p>' \
                                     '<img class=\"editor-inline-macro\" ' \
                                     'height=\"18\" width=\"88\" ' \
                                     'src=\"{base_url}/plugins/servlet/status-macro/placeholder?title=test\" ' \
                                     'data-macro-name=\"{macro_name}\" ' \
                                     'data-macro-parameters=\"{macro_parameters}\" ' \
                                     'data-macro-default-parameter=\"{default_parameters}\" ' \
                                     'data-macro-schema-version=\"1\" /></p>'
    return macro_to_editor_map


def get_macro_editor_format(test_script, base_url, macro_name, parameters='', default_parameters=''):
    return string.Formatter().vformat(macro_to_editor_mapping(test_script)[macro_name], (),
                                      defaultdict(str, base_url=base_url,
                                                  macro_name=macro_name,
                                                  macro_parameters=parameters,
                                                  default_parameters=default_parameters))


def get_random_macro_editor_format(test_script, page_id, params_dict=defaultdict(str), callback=None):
    """
    This method randomly pick macros by on the preconfig weight. The formula is select macro if
    random_number > macro['weight']
    :param test_script:
    :param page_id:
    :param params_dict:
    :param callback:
    :return: body with picked macros in editor format
    """
    score = randint(0, 100)
    editor_body = ''
    macro_list = []
    for macro in support_macro_list:
        if score > macro['weight']:
            continue

        if callback:
            callback(test_script, page_id, macro['name'])

        format_params = string.Formatter().vformat(macro['params'], (), params_dict)
        format_default_params = string.Formatter().vformat(macro['default_params'], (), params_dict)
        base_url = '' if not test_script or not test_script.test_data \
                         or not test_script.test_data.base_url else test_script.test_data.base_url
        editor_body += get_macro_editor_format(test_script, base_url,
                                               macro['name'], format_params, format_default_params)
        editor_body += "\n<br>"
        macro_list.append(macro['name'])

    return {"body": editor_body, "macros": macro_list}
