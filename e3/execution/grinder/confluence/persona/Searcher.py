import random

from confluence.common.helper.ConfluenceUserCreator import *
from confluence.common.helper.Authentication import login, logout
from confluence.common.helper.ResourceUtils import *
from confluence.common.helper.Utils import *
from confluence.common.wrapper.User import User

from TestScript import TestScript, is_http_ok

from net.grinder.script.Grinder import grinder


class Searcher(TestScript):

    def __init__(self, number, args):
        super(Searcher, self).__init__(number, args)
        self._current_user = None

        self._keyword_list = get_search_keywords()
        self._label_list = get_labels()
        self._quicknav_keyword_list = get_quick_nav_keywords()

    def __call__(self, *args, **kwargs):
        self._user_name = get_user_name()
        self._current_user = User(self._user_name, self._user_name)

        grinder.logger.info("Starting Searcher persona with user name %s" % self._user_name)
        safe_execute(self, self.logger, self._test, self.report_success)

    def _test(self):
        login(self, self._current_user)

        random_keyword_index = random.randint(0, len(self._keyword_list) - 1)
        random_label_index = random.randint(0, len(self._label_list) - 1)
        random_quicknav_keyword_index = random.randint(0, len(self._label_list) - 1)

        self.rest("GET", "/rest/quicknav/1/search?query=this&_=1491199429975")
        self.rest("GET", "/rest/quicknav/1/search?query=this+is&_=1491199429975")
        self.rest("GET", "/rest/quicknav/1/search?query=this+is+sample&_=1491199429975")

        self.http("GET", "/search/searchv3.action",
                  {
                      "queryString": self._keyword_list[random_keyword_index]["keyword"]
                  })

        self.http("GET", "/%s" % self._label_list[random_label_index]["label"])

        self.http("GET", "/json/contentnamesearch.action",
                  {"query": self._quicknav_keyword_list[random_quicknav_keyword_index]["keyword"]})
        if not is_http_ok():
            return False

        logout(self)
        return True
