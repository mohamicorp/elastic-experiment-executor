java -cp 'execution/lib/grinder-3.11/lib/*' -Dgrinder.logLevel=INFO -Dgrinder.console.httpHost=localhost net.grinder.Console &

java -cp 'execution/lib/grinder-3.11/lib/grinder.jar' \
                -Dcom.sun.management.jmxremote \
                -Dcom.sun.management.jmxremote.port=3333 \
                -Dcom.sun.management.jmxremote.authenticate=false \
                -Dcom.sun.management.jmxremote.ssl=false \
                -Dgrinder.consoleHost=localhost \
                -Dgrinder.jvm.arguments='-Dinstance=e3-local -Dworkload=bitbucket-mixed2 -DagentCount=1 -Dgrinder.logLevel=DEBUG -Droot=/home/andrey/work/mohamicorp/dc-readiness/elastic-experiment-executor/e3-home' \
                net.grinder.Grinder -daemon 5
