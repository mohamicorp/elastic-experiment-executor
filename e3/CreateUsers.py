#!/usr/bin/env python
import click
import json
import ntpath
import io
import requests
from requests.auth import HTTPBasicAuth


@click.command()
@click.option('-i', '--instance', required=True, help='Instance')
def command(instance):

    with io.open("../e3-home/instances/%s.json"%instance , encoding="utf-8") as f:
        instance_data = json.load(f)

    snapshot = instance_data['snapshot']

    with io.open("../e3-home/snapshots/%s.json"%snapshot , encoding="utf-8") as f:
        snapshot_data = json.load(f)

    users = snapshot_data["users"]
    projects = snapshot_data["projects"]
    baseurl = instance_data["URL"]

    admin_user = 'admin'

    admin_password = instance_data["RunConfig"]["admin_password"]

    print baseurl

    for user in users:
        print user['username']
        if 'password' in user:
            payload = {
                            "name" : user['username'],
                            "password" : user['password'],
                            "displayName" : user['username'],
                            "emailAddress" : "%s@aaa.aa"%user['username']
                          }
            #print payload

            r = requests.post('%s/rest/api/1.0/admin/users' % baseurl, timeout=60,
                        auth=HTTPBasicAuth(admin_user, admin_password),
                        params=payload,
                          headers={"Content-Type": "application/json"})
            #print r.text
            #if r.status_code == 204:
            for project in projects:
                #/REST/API/1.0/PROJECTS/{PROJECTKEY}/PERMISSIONS/USERS?NAME&PERMISSION

                r = requests.put('%s/rest/api/1.0/projects/%s/permissions/users' % (baseurl, project['name']), timeout=60,
                        auth=HTTPBasicAuth(admin_user, admin_password),
                        params={
                            "name" : user['username'],
                            "permission" : 'PROJECT_WRITE'
                        },
                        headers={"Content-Type": "application/json"})

        if 'public_key' in user:
            for project in projects:
                r = requests.post('%s/rest/keys/1.0/projects/%s/ssh' % (baseurl, project['name']), timeout=60,
                            auth=HTTPBasicAuth(admin_user, admin_password),
                            data=json.dumps({
                                    "key": {
                                        "text": user['public_key']
                                    },
                                    "permission": "PROJECT_WRITE"

                                }),
                            headers={"Content-Type": "application/json"})

                print r.text
                print r.status_code

if __name__ == '__main__':
    command()