import os
import re
from datetime import datetime
import csv
import MySQLdb
import MySQLdb.cursors

from common.E3 import e3

def stages_for_thread(thread, run_dir):
    stages = set()
    for dir in os.listdir(os.path.join(run_dir, thread)):
        if dir.startswith('worker-node-'):
            for stage in os.listdir(os.path.join(run_dir, thread, dir)):
                if stage.startswith('stage-'):
                    stages.add(stage)
    return sorted(stages)


def load_to_db(run, alias):
    print 'Start exporting to db ...'
    root = os.path.realpath(os.path.join(os.path.dirname(__file__), '..',  '..', 'e3-home'))
    
    run_dir = os.path.join(os.path.join(root, 'runs'), run)

    if not os.path.isdir(run_dir):
        print 'No such run "%s"' % run
        return

    data_files = get_data_files(run_dir)

    mysql_host = e3.get_mysql_host()
    mysql_db = e3.get_mysql_db()
    mysql_user = e3.get_mysql_user()
    mysql_password = e3.get_mysql_password()

    mysql_connection = MySQLdb.connect(host=mysql_host, user=mysql_user, passwd=mysql_password, db=mysql_db,
                                   cursorclass=MySQLdb.cursors.DictCursor, use_unicode=True, charset="utf8")
    try:
        try :
            mysql_cursor = mysql_connection.cursor()

            may_be_create_table(mysql_cursor)

            if not check_alias(mysql_cursor, alias):
                print "Data for alias %s already loaded - export cancelled"%(alias) 
                return

            for data_file in data_files:
               insert_data(mysql_connection, mysql_cursor, data_file, alias)

        finally:
            mysql_cursor.close()
    finally:
        print 'Finished db export'
        mysql_connection.close()


def may_be_create_table(mysql_cursor):
    mysql_cursor.execute("""
            CREATE TABLE IF NOT EXISTS `stats` (
              `alias` varchar(128) DEFAULT NULL,
              `test_id` varchar(128) DEFAULT NULL,
              `start_time` datetime DEFAULT NULL,
              `test_time` int(11) DEFAULT NULL,
              `errors` int(11) DEFAULT NULL,
              `http_response_code` int(11) DEFAULT NULL,
              `http_response_length` int(11) DEFAULT NULL,
              `time_to_resolve_host` int(11) DEFAULT NULL,
              `time_to_establish_connection` int(11) DEFAULT NULL,
              `time_to_first_byte` int(11) DEFAULT NULL,
              `new_connections` int(11) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        """)

def insert_data(mysql_connection, mysql_cursor, data_file, alias):
    log_file = re.sub("-0-data.log$","-0.log", data_file)

    test_mapping = get_test_mapping(log_file)

    with open(data_file, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        first_row = True
        for row in spamreader:
            if first_row:
                first_row = False
            else :
                numbers = map(lambda m: int(m), row)
                test_id = numbers[2]
                if test_id in test_mapping:
                    mysql_cursor.execute("""
                            insert into stats (alias, test_id, start_time, test_time, errors, 
                                http_response_code, http_response_length, time_to_resolve_host,
                                time_to_establish_connection, time_to_first_byte, new_connections)
                            values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                            (alias, test_mapping[test_id], datetime.utcfromtimestamp(numbers[3]/1e3), numbers[4], numbers[5],
                                numbers[6], numbers[7], numbers[8],
                                numbers[9], numbers[10], numbers[11]))

        mysql_connection.commit()

def get_data_files(run_dir):
    files = []

    threads = sorted([thread for thread in os.listdir(run_dir) if thread.startswith('thread-')])

    for thread in threads:
        grinder_files = []
        for dir in os.listdir(os.path.join(run_dir, thread)):
            if dir.startswith('worker-node-'):
                for stage in os.listdir(os.path.join(run_dir, thread, dir)):
                    if stage.startswith('stage-'):
                        stage_dir = os.path.join(run_dir, thread, dir, stage)
                        for file in os.listdir(stage_dir):
                            if file.endswith('-0-data.log'):
                                files.append(os.path.join(stage_dir, file))
    



    return files

def get_test_mapping(log_file):
    test_mapping = {}
    with open(log_file, 'r') as f:
        for line in f:
            m = re.match(r'Test ', line)
            if m:
                fields = re.split("[ \t]+", line)
                test_mapping[int(fields[1])] =  fields[13].lstrip('"').rstrip('\n').rstrip('"')

    return test_mapping

def get_test_names(run_dir):
    start = {}
    elapsed_time = {}

    threads = sorted([thread for thread in os.listdir(run_dir) if thread.startswith('thread-')])

    test_names = set()
    for thread in threads:
        for stage in stages_for_thread(thread, run_dir):
            # Each worker node may have a different idea of the start and elapsed time, even in the same experiment stage,
            # so gather all of them.
            for dir in os.listdir(os.path.join(run_dir, thread)):
                if dir.startswith('worker-node-'):
                    stage_dir = os.path.join(run_dir, thread, dir, stage)
                    for log_file in os.listdir(stage_dir):
                        if log_file.endswith('-0.log'):
                            with open(os.path.join(stage_dir, log_file)) as infile:
                                for line in infile:
                                    m = re.match(r'.*start time is ([0-9]*) ms since Epoch', line)
                                    if m:
                                        start[thread + ',' + stage] = int(m.group(1))
                                    m = re.match(r'.*elapsed time is ([0-9]*) ms', line)
                                    if m:
                                        elapsed_time[thread + ',' + stage] = int(m.group(1))
                                    m = re.match(r'Test [0-9]+', line)
                                    if m:
                                        test_names.add(line.split('"')[1])

    return test_names
def check_alias(mysql_cursor, alias):
    mysql_cursor.execute("""select count(*) cnt from stats where alias=%s""", (alias,))
    existing = mysql_cursor.fetchone()

    return existing['cnt'] == 0 
