#!/usr/bin/env python
import click
import MySQLdb
import MySQLdb.cursors
import xlsxwriter

from common.E3 import e3

def configure_worksheet(worksheet):
    worksheet.set_column(0, 0, 20) 
    worksheet.set_column(1, 1, 20) 
def get_work_sheet_key(row):
    return row['alias'] + '-' + row['test_id'] 

def make_report(mysql_cursor, test, filename, alias_prefix):
    data = loadData(mysql_cursor, test, alias_prefix)
    print len(data)

    key = get_work_sheet_key(data[0])

    workbook = xlsxwriter.Workbook(filename)
    date_format = workbook.add_format({'num_format': 'yyyy-mm-dd h:mm:ss'})

    worksheet = workbook.add_worksheet(key)
    configure_worksheet(worksheet)

    row = 0

    for i in range(0,len(data)):
        if not key == get_work_sheet_key(data[i]):
            key = get_work_sheet_key(data[i])
            worksheet = workbook.add_worksheet(key)
            configure_worksheet(worksheet)
            row = 0
        else:
            row+=1

        worksheet.write(row, 0, data[i]['test_id'])
        worksheet.write_datetime(row, 1, data[i]['start_time'], date_format)
        worksheet.write(row, 2, data[i]['test_time'])

    workbook.close()


def loadData(mysql_cursor, test, alias):
    aliasPlaceHolders = ",".join(map(lambda x: '%s', alias))
    testPlaceHolders = ",".join(map(lambda x: '%s', test))

    mysql_cursor.execute("""select
                                alias,
                                test_id,
                                start_time,
                                test_time
                            from stats 
                            where 
                                alias in (%s)
                                and test_id in (%s)
                            order by alias, test_id, start_time"""%(aliasPlaceHolders, testPlaceHolders), alias + test)
    existing = mysql_cursor.fetchall()

    return existing


@click.command()
@click.option('-t', '--test', multiple=True, required=True, help='Test name')
@click.option('-f', '--filename', required=True, help="XLS file for build report")
@click.option('-a', '--alias', multiple=True, help="Test alias prefix")

def command(test, filename, alias):
    print test
    print filename
    print alias

    mysql_host = e3.get_mysql_host()
    mysql_db = e3.get_mysql_db()
    mysql_user = e3.get_mysql_user()
    mysql_password = e3.get_mysql_password()

    mysql_connection = MySQLdb.connect(host=mysql_host, user=mysql_user, passwd=mysql_password, db=mysql_db,
                                   cursorclass=MySQLdb.cursors.DictCursor, use_unicode=True, charset="utf8")
    try:
        try :
            mysql_cursor = mysql_connection.cursor()

            make_report(mysql_cursor, test, filename, alias)

        finally:
            mysql_cursor.close()
    finally:
        mysql_connection.close()

if __name__ == "__main__":
    command()

